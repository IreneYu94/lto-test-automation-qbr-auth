when 'Open', { 'success' should: 'Ready for Release'}
when 'Open', { 'failure' should: 'Open'}
when 'Open', { 'pending' should: 'IN QA'}
when 'Open', { 'error' should: 'IN QA'}
when 'In Analysis', { 'success' should: 'Ready for Release'}
when 'In Analysis', { 'failure' should: 'Open'}
when 'In Analysis', { 'pending' should: 'IN QA'}
when 'In Analysis', { 'error' should: 'IN QA'}
when 'Ready', { 'success' should: 'Ready for Release'}
when 'Ready', { 'failure' should: 'Open'}
when 'Ready', { 'pending' should: 'IN QA'}
when 'Ready', { 'error' should: 'IN QA'}
when 'In Dev', { 'success' should: 'Ready for Release'}
when 'In Dev', { 'failure' should: 'Open'}
when 'In Dev', { 'pending' should: 'IN QA'}
when 'In Dev', { 'error' should: 'IN QA'}
when 'Ready for QA', { 'success' should: 'Ready for Release'}
when 'Ready for QA', { 'failure' should: 'Open'}
when 'Ready for QA', { 'pending' should: 'IN QA'}
when 'Ready for QA', { 'error' should: 'IN QA'}
when 'IN QA', { 'success' should: 'Ready for Release'}
when 'IN QA', { 'failure' should: 'Open'}
when 'IN QA', { 'pending' should: 'IN QA'}
when 'IN QA', { 'error' should: 'IN QA'}
when 'Ready for Release', { 'success' should: 'Ready for Release'}
when 'Ready for Release', { 'failure' should: 'Open'}
when 'Ready for Release', { 'pending' should: 'IN QA'}
when 'Ready for Release', { 'error' should: 'IN QA'}
when 'Closed', { 'success' should: 'Closed'}
when 'Closed', { 'failure' should: 'Open'}
when 'Closed', { 'pending' should: 'IN QA'}
when 'Closed', { 'error' should: 'IN QA'}