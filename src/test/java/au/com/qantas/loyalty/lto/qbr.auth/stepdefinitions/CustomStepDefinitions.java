package au.com.qantas.loyalty.lto.template.stepdefinitions;

import au.com.qantas.loyalty.lto.lib.SString;
import cucumber.api.java.en.Then;

public class CustomStepDefinitions {

	@Then("^display message (.*)$")
	public void sysout(SString special) throws Exception {
		System.out.println(special.str);
	}
}
