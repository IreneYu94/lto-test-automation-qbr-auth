package au.com.qantas.loyalty.lto.template;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features="src/test/resources/features/",

		glue = {
				"au.com.qantas.loyalty.lto.hooks",
				"au.com.qantas.loyalty.lto.stepdefinitions",
				"au.com.qantas.loyalty.lto.template.stepdefinitions"
		}

		)

public class StartExecution {

}
