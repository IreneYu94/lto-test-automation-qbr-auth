cd %CD%
cd ..\..
docker run -it --rm -v %CD%:/WORKDIR -w /WORKDIR qantasloyalty/lto-taf/chrome:latest /bin/bash -c "startupForDockerContainer;mvn versions:use-latest-versions -Dincludes=au.com.qantas.loyalty.lto:test-automation-framework-lib;mvn clean compile exec:java -Dexec.mainClass=au.com.qantas.loyalty.lto.jira.MapFeaturesToJIRA -Dexec.args=\"none\";mvn clean verify -Dwebdriver.driver=chrome -DexecEnv=sit"
pause