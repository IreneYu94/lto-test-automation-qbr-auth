cd %CD%
cd ..\..
docker run -it --rm -v %CD%:/WORKDIR -w /WORKDIR qantasloyalty/lto-taf/api:latest /bin/bash -c "mvn versions:use-latest-versions -Dincludes=au.com.qantas.loyalty.lto:test-automation-framework-lib;mvn clean compile exec:java -Dexec.mainClass=au.com.qantas.loyalty.lto.jira.MapFeaturesToJIRA -Dexec.args=\"none\";mvn clean verify -DexecEnv=sit -Dcucumber.options=\"--tags @api\""
pause